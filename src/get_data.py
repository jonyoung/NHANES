#!/usr/bin/env python

"""
Usage: 

Download SAS XPT files for National Health and Nutrition Examination Survey 
(NHANES) from CDC's National Center for Health Statistics. URLs of data files 
are listed in 

Adapted from NHANES-Downloader
Copyright (c)
    2017 by The University of Delaware
    Contributors: Michael Wyatt
    Affiliation: Global Computing Laboratory, Michela Taufer PI
    URL: http://glc.cis.udel.edu/, https://github.com/TauferLab
"""

import argparse
import re
import urllib.request
from bs4 import BeautifulSoup


def parse_page_XPT(htmlSource):
    """Finds all links to XPT files in source HTML"""
    soup = BeautifulSoup(htmlSource, 'html.parser')
    xptURLs = soup.find_all('a', href=re.compile('\.XPT$'))
    xptURLs = [url['href'] for url in xptURLs]
    return xptURLs


def parse_website(url):
    """Reads HTML source from URL, parses HTML for XPT files, and saves files"""
    baseURL = re.search(r'(h.+://[\w.]+)/', url).group(1)
    with urllib.request.urlopen(url) as page:
        htmlSource = page.read()
    fileURLs = parse_page_XPT(htmlSource)
    fileURLs = [baseURL + url for url in fileURLs]


def main():
    # Get text file with list of URLs for NHANES data
    parser = argparse.ArgumentParser(description='Download NHANES XPT')
    parser.add_argument('url_list', type=str, nargs='?', 
            help='text document containing URLs to NHANES website')
    args = parser.parse_args()

    # Get list of URLs
    with open(args.url_list, 'r') as f:
        urls = f.readlines()
    
    # Parse each webpage
    for url in urls:
        parse_website(url)


if __name__=='__main__':
    main()
